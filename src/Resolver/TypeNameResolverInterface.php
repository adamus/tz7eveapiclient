<?php

/*
 * This file is part of the Tz7\EveApiBundle package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Resolver;


interface TypeNameResolverInterface
{
    const TYPE_LOCATION_NAME = 'location_name';
    const TYPE_ENTITY_NAME   = 'entity_name';
    const TYPE_ITEM_NAME     = 'item_name';

    const TYPE_LOCATION_ID = 'location_id';
    const TYPE_ENTITY_ID   = 'entity_id';
    const TYPE_ITEM_ID     = 'item_id';

    /**
     * @param integer $id
     *
     * @return string
     */
    public function getLocationName($id);

    /**
     * @param string $name
     *
     * @return int
     */
    public function getLocationId($name);

    /**
     * @param integer $id
     *
     * @return string
     */
    public function getEntityName($id);

    /**
     * @param string $name
     *
     * @return int
     */
    public function getEntityId($name);

    /**
     * @param integer $id
     *
     * @return string
     */
    public function getItemName($id);

    /**
     * @param string $name
     *
     * @return int
     */
    public function getItemId($name);
}
