<?php

/*
 * This file is part of the Tz7\EveApiBundle package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Resolver;


interface NotificationResolverInterface
{
    /**
     * @param int   $typeId
     * @param array $parameters
     *
     * @return string
     */
    public function resolve($typeId, array $parameters);
}
