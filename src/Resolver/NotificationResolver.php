<?php

/*
 * This file is part of the Tz7\EveApiBundle package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Resolver;


use Tz7\EveApiClient\Provider\ExpressionParserProvider;


class NotificationResolver implements NotificationResolverInterface
{
    /** @var ExpressionParserProvider */
    protected $expressionParser;

    /** @var array */
    protected $typeMap;

    /**
     * @param ExpressionParserProvider $expressionParser
     * @param array                    $typeMap
     */
    public function __construct(ExpressionParserProvider $expressionParser, array $typeMap)
    {
        $this->expressionParser = $expressionParser;
        $this->typeMap          = $typeMap;
    }

    /**
     * @param int   $typeId
     * @param array $parameters
     *
     * @return string
     */
    public function resolve($typeId, array $parameters)
    {
        $rawLabel    = $this->getLabelForNotification($typeId);
        $information = $this->getInformationForNotification($parameters);

        if (empty($information))
        {
            return $rawLabel;
        }
        else
        {
            return $this->expressionParser->processBlocks($rawLabel, $information);
        }
    }

    /**
     * @param int    $typeId
     * @param string $default
     *
     * @return string
     */
    protected function getLabelForNotification($typeId, $default = "")
    {
        if (isset($this->typeMap[$typeId]['label']))
        {
            return $this->typeMap[$typeId]['label'];
        }
        else
        {
            return $default;
        }
    }

    /**
     * @param array $parameters
     *
     * @return array
     */
    protected function getInformationForNotification(array $parameters)
    {
        $resolveMap = [
            'amount'      => [
                function ($amount)
                {
                    return number_format((float) $amount, 0, '.', ' ');
                }
            ],
            'decloakTime' => [
                function ($stamp)
                {
                    return date('Y-m-d', round($stamp / 10000000) - 11644473600);
                }
            ],
            'dueDate'     => [
                function ($stamp)
                {
                    return date('Y-m-d', round($stamp / 10000000) - 11644473600);
                }
            ],
            'timeLeft'    => [
                function ($stamp)
                {
                    return date('Y-m-d', time() + (round($stamp / 10000000) - 11644473600));
                }
            ]
        ];

        $information = [];
        foreach ($parameters as $key => $value)
        {
            if (isset($resolveMap[$key]))
            {
                $resolved = null;
                foreach ($resolveMap[$key] as $resolver)
                {
                    try
                    {
                        $resolved = (string) call_user_func($resolver, $value);
                        if ($resolved)
                        {
                            break;
                        }
                    }
                    catch (\Exception $ex)
                    {
                    }
                }
                if ($resolved)
                {
                    $information[rtrim($key, 'ID')] = $resolved;
                }
            }
            else
            {
                $information[$key] = $value;
            }
        }

        return $information;
    }

    /**
     * @param       $label
     * @param array $information
     */
    protected function replaceLabel(&$label, array &$information)
    {
        $replaceMap = [
            'char' => [
                'character',
                'char'
            ],
            'corp' => [
                'corporation',
                'corp'
            ]
        ];

        foreach ($information as $key => $value)
        {
            if (isset($replaceMap[$key]))
            {
                foreach ($replaceMap[$key] as $search)
                {
                    $pattern = sprintf('/(%s)/i', $search);
                    if (preg_match($pattern, $label))
                    {
                        $label = preg_replace($pattern, $value, $label);
                        unset($information[$key]);
                        break;
                    }
                }
            }
        }
    }
}
