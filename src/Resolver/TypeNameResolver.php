<?php

/*
 * This file is part of the Tz7\EveApiBundle package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Resolver;


use Tz7\EveApiClient\Adapter\PublicFunctionAdapterInterface;


/*
 * You might want to override this class and use some caching.
 */
class TypeNameResolver implements TypeNameResolverInterface
{
    /** @var PublicFunctionAdapterInterface */
    protected $publicFunctionAdapter;

    /**
     * @param PublicFunctionAdapterInterface $publicFunctionAdapter
     */
    public function __construct(PublicFunctionAdapterInterface $publicFunctionAdapter)
    {
        $this->publicFunctionAdapter = $publicFunctionAdapter;
    }

    /**
     * @param integer $id
     * @return string
     */
    public function getLocationName($id)
    {
        return $this->readType(static::TYPE_LOCATION_NAME, $id);
    }

    /**
     * @param string $name
     * @return int
     */
    public function getLocationId($name)
    {
        return intval($this->readType(static::TYPE_LOCATION_ID, $name));
    }

    /**
     * @param integer $id
     * @return string
     */
    public function getEntityName($id)
    {
        return $this->readType(static::TYPE_ENTITY_NAME, $id);
    }

    /**
     * @param string $name
     * @return int
     */
    public function getEntityId($name)
    {
        return intval($this->readType(static::TYPE_ENTITY_ID, $name));
    }

    /**
     * @param integer $id
     * @return string
     */
    public function getItemName($id)
    {
        return $this->readType(static::TYPE_ITEM_NAME, $id);
    }

    /**
     * @param string $name
     * @return int
     */
    public function getItemId($name)
    {
        return intval($this->readType(static::TYPE_ITEM_ID, $name));
    }

    /**
     * @param string $type
     * @param mixed $key
     * @return mixed
     */
    protected function readType($type, $key)
    {
        $idToName = [static::TYPE_LOCATION_NAME, static::TYPE_ENTITY_NAME, static::TYPE_ITEM_NAME];
        $nameToId = [static::TYPE_LOCATION_ID, static::TYPE_ENTITY_ID, static::TYPE_ITEM_ID];

        if (in_array($type, $idToName))
        {
            switch ($type)
            {
                case static::TYPE_LOCATION_NAME:
                case static::TYPE_ENTITY_NAME:
                    return $this->publicFunctionAdapter->getEntityName($key);

                case static::TYPE_ITEM_NAME:
                    return $this->publicFunctionAdapter->getItemName($key);

                default:
                    throw new \InvalidArgumentException(sprintf('Invalid type "%s"', $type));
            }
        }
        elseif (in_array($type, $nameToId))
        {
            switch ($type)
            {
                case static::TYPE_LOCATION_ID:
                case static::TYPE_ENTITY_ID:
                    return $this->publicFunctionAdapter->getEntityId($key);

                case static::TYPE_ITEM_ID:
                    return $this->publicFunctionAdapter->getItemId($key);

                default:
                    throw new \InvalidArgumentException(sprintf('Invalid type "%s"', $type));
            }
        }
        else
        {
            throw new \InvalidArgumentException(sprintf('Invalid type "%s"', $type));
        }
    }
}
