<?php

/*
 * This file is part of the Tz7\EveApiClient package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Model;


use DateTimeInterface;
use DateTimeImmutable;
use DateTimeZone;


abstract class AbstractModelFactory
{
    /**
     * @param DateTimeInterface|string $value
     *
     * @return DateTimeInterface
     */
    protected function createLocalTime($value)
    {
        if ($value instanceof DateTimeInterface)
        {
            return $value;
        }

        if (!$value)
        {
            return null;
        }

        $dateTime = new DateTimeImmutable($value, new DateTimeZone('UTC'));
        $dateTime->setTimezone((new DateTimeImmutable)->getTimezone());

        return $dateTime;
    }
}
