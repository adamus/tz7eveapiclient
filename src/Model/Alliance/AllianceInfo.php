<?php

/*
 * This file is part of the Tz7\EveApiClient package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Model\Alliance;


use DateTimeInterface;
use Tz7\EveApiClient\Model\ApiResult;


class AllianceInfo extends ApiResult
{
    /** @var int */
    private $allianceID;

    /** @var string */
    private $name;

    /** @var string */
    private $shortName;

    /** @var int */
    private $executorCorpID;

    /** @var DateTimeInterface */
    private $startDate;

    /**
     * @param int               $allianceID
     * @param string            $name
     * @param string            $shortName
     * @param int               $executorCorpID
     * @param DateTimeInterface $startDate
     * @param DateTimeInterface $cachedUntil
     */
    public function __construct(
        $allianceID,
        $name,
        $shortName,
        $executorCorpID,
        DateTimeInterface $startDate,
        DateTimeInterface $cachedUntil = null
    ) {
        $this->allianceID     = $allianceID;
        $this->name           = $name;
        $this->shortName      = $shortName;
        $this->executorCorpID = $executorCorpID;
        $this->startDate      = $startDate;
        $this->cachedUntil    = $cachedUntil;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getShortName()
    {
        return $this->shortName;
    }

    /**
     * @return int
     */
    public function getAllianceID()
    {
        return intval($this->allianceID);
    }

    /**
     * @return int
     */
    public function getExecutorCorpID()
    {
        return intval($this->executorCorpID);
    }

    /**
     * @return DateTimeInterface
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize()
    {
        return [
            'name'           => $this->name,
            'shortName'      => $this->shortName,
            'allianceID'     => $this->allianceID,
            'executorCorpID' => $this->executorCorpID,
            'startDate'      => $this->startDate->format('Y-m-d H:i:s')
        ];
    }
}