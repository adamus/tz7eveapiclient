<?php

/*
 * This file is part of the Tz7\EveApiClient package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Model\Alliance;


use DateTimeInterface;
use Tz7\EveApiClient\Model\AbstractModelFactory;


class AllianceModelFactory extends AbstractModelFactory
{
    /**
     * @param int                      $allianceID
     * @param string                   $name
     * @param string                   $shortName
     * @param int                      $executorCorpID
     * @param DateTimeInterface|string $startDate
     *
     * @return AllianceInfo
     */
    public function createAllianceInfo($allianceID, $name, $shortName, $executorCorpID, $startDate)
    {
        return new AllianceInfo($allianceID, $name, $shortName, $executorCorpID, $this->createLocalTime($startDate));
    }
}
