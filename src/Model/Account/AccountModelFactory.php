<?php

/*
 * This file is part of the Tz7\EveApiClient package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Model\Account;


use DateTimeInterface;
use Tz7\EveApiClient\Model\AbstractModelFactory;
use Tz7\EveApiClient\Model\Character\CharacterInfo;


class AccountModelFactory extends AbstractModelFactory
{
    /**
     * @param CharacterInfo[]          $characters
     * @param DateTimeInterface|string $cachedUntil
     *
     * @return AccountCharacters
     */
    public function createAccountCharacters(array $characters, $cachedUntil)
    {
        return new AccountCharacters($characters, $this->createLocalTime($cachedUntil));
    }

    /**
     * @param DateTimeInterface|string $paidUntil
     * @param DateTimeInterface|string $createDate
     * @param int                      $logonCount
     * @param int                      $logonMinutes
     * @param DateTimeInterface|string $cachedUntil
     *
     * @return AccountStatus
     */
    public function createAccountStatus($paidUntil, $createDate, $logonCount, $logonMinutes, $cachedUntil)
    {
        return new AccountStatus(
            $this->createLocalTime($paidUntil),
            $this->createLocalTime($createDate),
            $logonCount,
            $logonMinutes,
            $this->createLocalTime($cachedUntil)
        );
    }

    /**
     * @param DateTimeInterface|string $expires
     * @param string                   $type
     * @param int                      $accessMask
     * @param array                    $characters
     * @param DateTimeInterface|string $cachedUntil
     *
     * @return ApiKeyInfo
     */
    public function createApiKeyInfo($expires, $type, $accessMask, array $characters, $cachedUntil)
    {
        return new ApiKeyInfo(
            $this->createLocalTime($expires),
            $type,
            $accessMask,
            $characters,
            $this->createLocalTime($cachedUntil)
        );
    }
}
