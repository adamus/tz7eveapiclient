<?php

/*
 * This file is part of the Tz7\EveApiClient package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Model\Account;


use DateTimeInterface;
use Tz7\EveApiClient\Model\ApiResult;
use Tz7\EveApiClient\Model\Character\CharacterInfo;


class AccountCharacters extends ApiResult
{
    /** @var CharacterInfo[] */
    private $characters = [];

    /**
     * @param CharacterInfo[]   $characters
     * @param DateTimeInterface $cachedUntil
     */
    public function __construct(array $characters, DateTimeInterface $cachedUntil = null)
    {
        $this->characters  = $characters;
        $this->cachedUntil = $cachedUntil;
    }

    /**
     * @return CharacterInfo[]
     */
    public function getCharacters()
    {
        return $this->characters;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'characters'  => $this->getCharacters(),
            'cachedUntil' => $this->formatDateTime($this->cachedUntil)
        ];
    }
}
