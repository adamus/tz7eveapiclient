<?php

/*
 * This file is part of the Tz7\EveApiClient package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Model\Account;


use DateTimeInterface;
use Tz7\EveApiClient\Model\ApiResult;


class AccountStatus extends ApiResult
{
    /** @var DateTimeInterface */
    private $paidUntil;

    /** @var DateTimeInterface */
    private $createDate;

    /** @var int */
    private $logonCount;

    /** @var int */
    private $logonMinutes;

    /**
     * @param DateTimeInterface $paidUntil
     * @param DateTimeInterface $createDate
     * @param int               $logonCount
     * @param int               $logonMinutes
     * @param DateTimeInterface $cachedUntil
     */
    public function __construct(
        DateTimeInterface $paidUntil,
        DateTimeInterface $createDate,
        $logonCount,
        $logonMinutes,
        DateTimeInterface $cachedUntil = null
    ) {
        $this->paidUntil    = $paidUntil;
        $this->createDate   = $createDate;
        $this->logonCount   = $logonCount;
        $this->logonMinutes = $logonMinutes;
        $this->cachedUntil  = $cachedUntil;
    }

    /**
     * @return DateTimeInterface
     */
    public function getPaidUntil()
    {
        return $this->paidUntil;
    }

    /**
     * @return DateTimeInterface
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * @return int
     */
    public function getLogonCount()
    {
        return $this->logonCount;
    }

    /**
     * @return int
     */
    public function getLogonMinutes()
    {
        return $this->logonMinutes;
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize()
    {
        return [
            'paidUntil'    => $this->paidUntil->format('Y-m-d H:i:s'),
            'createDate'   => $this->createDate->format('Y-m-d H:i:s'),
            'logonCount'   => $this->logonCount,
            'logonMinutes' => $this->logonMinutes,
            'cachedUntil'  => $this->getFormattedCachedUntil()
        ];
    }
}
