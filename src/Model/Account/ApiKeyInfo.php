<?php

/*
 * This file is part of the Tz7\EveApiClient package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Model\Account;


use DateTimeInterface;
use Tz7\EveApiClient\Model\Character\CharacterInfo;


/**
 * @deprecated Will be removed with ESI by CCP.
 */
class ApiKeyInfo extends AccountCharacters
{
    /** @var DateTimeInterface */
    protected $expires;

    /** @var string */
    protected $type;

    /** @var int */
    protected $accessMask;

    /**
     * @param DateTimeInterface $expires
     * @param string            $type
     * @param int               $accessMask
     * @param CharacterInfo[]   $characters
     * @param DateTimeInterface $cachedUntil
     */
    public function __construct(
        DateTimeInterface $expires,
        $type,
        $accessMask,
        array $characters,
        DateTimeInterface $cachedUntil = null
    ) {
        parent::__construct($characters, $cachedUntil);

        $this->expires    = $expires;
        $this->type       = $type;
        $this->accessMask = $accessMask;
    }

    /**
     * @return DateTimeInterface
     */
    public function getExpires()
    {
        return $this->expires;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return int
     */
    public function getAccessMask()
    {
        return $this->accessMask;
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize()
    {
        return array_merge(
            parent::jsonSerialize(),
            [
                'expires'    => $this->formatDateTime($this->expires),
                'type'       => $this->type,
                'accessMask' => $this->accessMask
            ]
        );
    }
}
