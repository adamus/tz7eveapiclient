<?php

/*
 * This file is part of the Tz7\EveApiClient package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Model;

use DateTimeInterface;

abstract class ApiResult implements \JsonSerializable
{
    /** @var DateTimeInterface */
    protected $cachedUntil;

    /**
     * @return null|string
     */
    public function getFormattedCachedUntil()
    {
        return $this->formatDateTime($this->cachedUntil);
    }

    /**
     * @return DateTimeInterface
     */
    public function getCachedUntil()
    {
        return $this->cachedUntil;
    }

    /**
     * @param DateTimeInterface $dateTime
     *
     * @return null|string
     */
    protected function formatDateTime(DateTimeInterface $dateTime = null)
    {
        return $dateTime instanceof DateTimeInterface ? $dateTime->format('Y-m-d H:i:s') : null;
    }
}
