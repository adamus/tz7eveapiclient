<?php

/*
 * This file is part of the Tz7\EveApiClient package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Model\Character;


use DateTimeInterface;
use Tz7\EveApiClient\Model\AbstractModelFactory;
use Tz7\EveApiClient\Util\Yaml\Yaml;


class CharacterModelFactory extends AbstractModelFactory
{
    /**
     * @param int                      $characterID
     * @param string                   $characterName
     * @param int                      $corporationID
     * @param EmploymentHistory[]      $employmentHistory
     * @param DateTimeInterface|string $cachedUntil
     *
     * @return CharacterInfo
     */
    public function createCharacterInfo(
        $characterID,
        $characterName,
        $corporationID,
        array $employmentHistory,
        $cachedUntil
    ) {
        $this->normalizeEmploymentHistory($employmentHistory);

        return new CharacterInfo(
            $characterID,
            $characterName,
            $corporationID,
            $employmentHistory,
            $this->createLocalTime($cachedUntil)
        );
    }

    /**
     * @param int                      $characterID
     * @param string                   $name
     * @param int                      $homeStationID
     * @param DateTimeInterface|string $dateOfBirth
     * @param string                   $race
     * @param string                   $bloodLine
     * @param string                   $ancestry
     * @param string                   $gender
     * @param string                   $corporationName
     * @param int                      $corporationID
     * @param string                   $allianceName
     * @param int                      $allianceID
     * @param string                   $factionName
     * @param int                      $factionID
     * @param string                   $cloneName
     * @param int                      $freeSkillPoints
     * @param int                      $freeRespecs
     * @param DateTimeInterface|string $cloneJumpDate
     * @param DateTimeInterface|string $lastRespecDate
     * @param DateTimeInterface|string $lastTimedRespec
     * @param DateTimeInterface|string $remoteStationDate
     * @param DateTimeInterface|string $jumpActivation
     * @param DateTimeInterface|string $jumpFatigue
     * @param DateTimeInterface|string $jumpLastUpdate
     * @param float                    $balance
     * @param CorporationRole[]        $corporationRoles
     * @param DateTimeInterface|string $cachedUntil
     *
     * @return CharacterSheet
     */
    public function createCharacterSheet(
        $characterID,
        $name,
        $homeStationID,
        DateTimeInterface $dateOfBirth,
        $race,
        $bloodLine,
        $ancestry,
        $gender,
        $corporationName,
        $corporationID,
        $allianceName,
        $allianceID,
        $factionName,
        $factionID,
        $cloneName,
        $freeSkillPoints,
        $freeRespecs,
        $cloneJumpDate,
        $lastRespecDate,
        $lastTimedRespec,
        $remoteStationDate,
        $jumpActivation,
        $jumpFatigue,
        $jumpLastUpdate,
        $balance,
        array $corporationRoles,
        $cachedUntil
    ) {
        return new CharacterSheet(
            $characterID,
            $name,
            $homeStationID,
            $this->createLocalTime($dateOfBirth),
            $race,
            $bloodLine,
            $ancestry,
            $gender,
            $corporationName,
            $corporationID,
            $allianceName,
            $allianceID,
            $factionName,
            $factionID,
            $cloneName,
            $freeSkillPoints,
            $freeRespecs,
            $this->createLocalTime($cloneJumpDate),
            $this->createLocalTime($lastRespecDate),
            $this->createLocalTime($lastTimedRespec),
            $this->createLocalTime($remoteStationDate),
            $this->createLocalTime($jumpActivation),
            $this->createLocalTime($jumpFatigue),
            $this->createLocalTime($jumpLastUpdate),
            $balance,
            $corporationRoles,
            $this->createLocalTime($cachedUntil)
        );
    }

    /**
     * @param int    $roleID
     * @param string $roleName
     *
     * @return CorporationRole
     */
    public function createCorporationRole($roleID, $roleName)
    {
        return new CorporationRole($roleID, $roleName);
    }

    /**
     * @param int                      $recordID
     * @param int                      $corporationID
     * @param DateTimeInterface|string $startDate
     *
     * @return EmploymentHistory
     */
    public function createEmploymentHistory($recordID, $corporationID, $startDate)
    {
        return new EmploymentHistory($recordID, $corporationID, $this->createLocalTime($startDate));
    }

    /**
     * @param int                      $notificationID
     * @param int                      $typeID
     * @param int                      $senderID
     * @param string                   $senderName
     * @param DateTimeInterface|string $sentDate
     * @param bool                     $read
     *
     * @return Notification
     */
    public function createNotification(
        $notificationID,
        $typeID,
        $senderID,
        $senderName,
        $sentDate,
        $read
    ) {
        return new Notification($notificationID, $typeID, $senderID, $senderName, $this->createLocalTime($sentDate), $read);
    }

    /**
     * @param Notification[]           $notifications
     * @param DateTimeInterface|string $cachedUntil
     *
     * @return Notifications
     */
    public function createNotifications(array $notifications, $cachedUntil)
    {
        return new Notifications($notifications, $this->createLocalTime($cachedUntil));
    }

    /**
     * @param int    $notificationID
     * @param string $yaml
     *
     * @return NotificationText
     */
    public function createNotificationText($notificationID, $yaml)
    {
        return new NotificationText($notificationID, Yaml::parse(str_replace('<br>', "\n", $yaml)));
    }

    /**
     * @param NotificationText[]       $notificationTexts
     * @param DateTimeInterface|string $cachedUntil
     *
     * @return NotificationTexts
     */
    public function createNotificationTexts(array $notificationTexts, $cachedUntil)
    {
        return new NotificationTexts($notificationTexts, $this->createLocalTime($cachedUntil));
    }

    /**
     * @param int    $listID
     * @param string $displayName
     *
     * @return MailingList
     */
    public function createMailingList($listID, $displayName)
    {
        return new MailingList($listID, $displayName);
    }

    /**
     * @param MailingList[]            $mailingLists
     * @param DateTimeInterface|string $cachedUntil
     *
     * @return MailingLists
     */
    public function createMailingLists(array $mailingLists, $cachedUntil)
    {
        return new MailingLists($mailingLists, $this->createLocalTime($cachedUntil));
    }

    /**
     * @param int                      $messageID
     * @param int                      $senderID
     * @param string                   $senderName
     * @param DateTimeInterface|string $sentDate
     * @param string                   $title
     * @param int                      $toCorpOrAllianceID
     * @param int[]                    $toCharacterIDs
     * @param int                      $toListID
     *
     * @return MailMessage
     */
    public function createMailMessage(
        $messageID,
        $senderID,
        $senderName,
        $sentDate,
        $title,
        $toCorpOrAllianceID,
        $toCharacterIDs,
        $toListID
    ) {
        return new MailMessage(
            $messageID,
            $senderID,
            $senderName,
            $this->createLocalTime($sentDate),
            $title,
            $toCorpOrAllianceID,
            $toCharacterIDs,
            $toListID
        );
    }

    /**
     * @param MailMessage[]            $messages
     * @param DateTimeInterface|string $cachedUntil
     *
     * @return MailMessages
     */
    public function createMailMessages(array $messages, $cachedUntil)
    {
        return new MailMessages($messages, $this->createLocalTime($cachedUntil));
    }

    /**
     * @param int    $messageID
     * @param string $messageText
     *
     * @return MailBody
     */
    public function createMailBody($messageID, $messageText)
    {
        return new MailBody($messageID, $messageText);
    }

    /**
     * @param MailBody[]               $mailBodies
     * @param DateTimeInterface|string $cachedUntil
     *
     * @return MailBodies
     */
    public function createMailBodies($mailBodies, $cachedUntil)
    {
        return new MailBodies($mailBodies, $this->createLocalTime($cachedUntil));
    }

    /**
     * @param EmploymentHistory[] $employmentHistory
     */
    private function normalizeEmploymentHistory(array &$employmentHistory)
    {
        usort(
            $employmentHistory,
            function (EmploymentHistory $a, EmploymentHistory $b)
            {
                return $a->getStartDate() > $b->getStartDate();
            }
        );

        /** @var EmploymentHistory $lastEmployment */
        $lastEmployment = null;
        foreach ($employmentHistory as $employment)
        {
            if ($lastEmployment)
            {
                $lastEmployment->setEndDate(clone $employment->getStartDate());
            }
            $lastEmployment = $employment;
        }
    }
}
