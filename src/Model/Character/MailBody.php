<?php

/*
 * This file is part of the Tz7\EveApiClient package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Model\Character;


use Tz7\EveApiClient\Model\ApiResult;


class MailBody extends ApiResult
{
    /** @var int */
    private $messageID;

    /** @var string */
    private $messageText;

    /**
     * @param int    $messageID
     * @param string $messageText
     */
    public function __construct($messageID, $messageText)
    {
        $this->messageID   = $messageID;
        $this->messageText = $messageText;
    }

    /**
     * @return int
     */
    public function getMessageID()
    {
        return intval($this->messageID);
    }

    /**
     * @return string
     */
    public function getMessageText()
    {
        return $this->messageText;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'messageID'   => $this->messageID,
            'messageText' => $this->messageText
        ];
    }
}
