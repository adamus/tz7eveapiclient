<?php

/*
 * This file is part of the Tz7\EveApiClient package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Model\Character;


use DateTimeInterface;
use Tz7\EveApiClient\Model\ApiResult;


class MailMessages extends ApiResult
{
    /** @var MailMessage[] */
    private $messages = [];

    /**
     * @param MailMessage[]     $messages
     * @param DateTimeInterface $cachedUntil
     */
    public function __construct(array $messages, DateTimeInterface $cachedUntil = null)
    {
        $this->messages    = $messages;
        $this->cachedUntil = $cachedUntil;
    }

    /**
     * @return MailMessage[]
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @return int[]
     */
    public function getMessageIds()
    {
        return array_map(
            function (MailMessage $message)
            {
                return $message->getMessageID();
            },
            $this->getMessages()
        );
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize()
    {
        return [
            'messages'    => $this->messages,
            'cachedUntil' => $this->formatDateTime($this->cachedUntil)
        ];
    }
}
