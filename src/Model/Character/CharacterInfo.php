<?php

/*
 * This file is part of the Tz7\EveApiClient package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Model\Character;


use DateTimeInterface;
use Tz7\EveApiClient\Model\ApiResult;


class CharacterInfo extends ApiResult
{
    /** @var int */
    protected $characterID;

    /** @var string */
    protected $characterName;

    /** @var int */
    protected $corporationID;

    /** @var EmploymentHistory[] */
    protected $employmentHistory = [];

    /**
     * @param int                 $characterID
     * @param string              $characterName
     * @param int                 $corporationID
     * @param EmploymentHistory[] $employmentHistory
     * @param DateTimeInterface   $cachedUntil
     */
    public function __construct(
        $characterID,
        $characterName,
        $corporationID,
        array $employmentHistory,
        DateTimeInterface $cachedUntil = null
    ) {
        $this->characterID       = $characterID;
        $this->characterName     = $characterName;
        $this->corporationID     = $corporationID;
        $this->employmentHistory = $employmentHistory;
        $this->cachedUntil       = $cachedUntil;
    }

    /**
     * @return integer
     */
    public function getCharacterID()
    {
        return intval($this->characterID);
    }

    /**
     * @return string
     */
    public function getCharacterName()
    {
        return $this->characterName;
    }

    /**
     * @return integer
     */
    public function getCorporationID()
    {
        return intval($this->corporationID);
    }

    /**
     * @return EmploymentHistory[]
     */
    public function getEmploymentHistory()
    {
        return $this->employmentHistory;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'characterID'       => $this->characterID,
            'characterName'     => $this->characterName,
            'corporationID'     => $this->corporationID,
            'employmentHistory' => $this->employmentHistory,
            'cachedUntil'       => $this->getFormattedCachedUntil()
        ];
    }
}
