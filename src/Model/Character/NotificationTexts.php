<?php

/*
 * This file is part of the Tz7\EveApiClient package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Model\Character;


use DateTimeInterface;
use Tz7\EveApiClient\Model\ApiResult;


class NotificationTexts extends ApiResult
{
    /** @var NotificationText[] */
    private $notifications;

    /**
     * @param NotificationText[] $notifications
     * @param DateTimeInterface  $cachedUntil
     */
    public function __construct(array $notifications, DateTimeInterface $cachedUntil = null)
    {
        $this->notifications = $notifications;
        $this->cachedUntil   = $cachedUntil;
    }

    /**
     * @return NotificationText[]
     */
    public function getNotificationTexts()
    {
        return $this->notifications;
    }

    /**
     * @param int $notificationID
     *
     * @return bool
     */
    public function hasNotificationText($notificationID)
    {
        return isset($this->notifications[$notificationID]);
    }

    /**
     * @param int $notificationID
     *
     * @return null|NotificationText
     */
    public function getNotificationText($notificationID)
    {
        return $this->hasNotificationText($notificationID) ? $this->notifications[$notificationID] : null;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'notifications' => $this->notifications,
            'cachedUntil'   => $this->formatDateTime($this->cachedUntil)
        ];
    }
}
