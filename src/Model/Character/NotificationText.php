<?php

/*
 * This file is part of the Tz7\EveApiClient package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Model\Character;


use Tz7\EveApiClient\Model\ApiResult;


class NotificationText extends ApiResult
{
    /** @var int */
    private $notificationID;

    /** @var array */
    private $parameters = [];

    /**
     * @param int    $notificationID
     * @param array  $parameters
     */
    public function __construct($notificationID, array $parameters)
    {
        $this->notificationID = $notificationID;
        $this->parameters     = $parameters;
    }

    /**
     * @return int
     */
    public function getNotificationID()
    {
        return $this->notificationID;
    }

    /**
     * @return array
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'notificationID' => $this->notificationID,
            'parameters'     => $this->parameters
        ];
    }
}
