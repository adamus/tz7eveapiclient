<?php

/*
 * This file is part of the Tz7\EveApiClient package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Model\Character;


use DateTimeInterface;
use Tz7\EveApiClient\Model\ApiResult;


class Notifications extends ApiResult
{
    /** @var Notification[] */
    protected $notifications = [];

    /**
     * @param Notification[]    $notifications
     * @param DateTimeInterface $cachedUntil
     */
    public function __construct(array $notifications, DateTimeInterface $cachedUntil = null)
    {
        foreach ($notifications as $notification)
        {
            $this->notifications[$notification->getNotificationID()] = $notification;
        }

        $this->cachedUntil = $cachedUntil;
    }

    /**
     * @return Notification[]
     */
    public function getNotifications()
    {
        return $this->notifications;
    }

    /**
     * @param bool $onlyUnread
     *
     * @return array
     */
    public function getNotificationIds($onlyUnread = false)
    {
        return array_map(
            function (Notification $notification)
            {
                return $notification->getNotificationID();
            },
            array_filter(
                $this->getNotifications(),
                function (Notification $notification) use ($onlyUnread)
                {
                    return !$onlyUnread || !$notification->getRead();
                }
            )
        );
    }

    /**
     * @param int $notificationID
     *
     * @return bool
     */
    public function hasNotification($notificationID)
    {
        return isset($this->notifications[$notificationID]);
    }

    /**
     * @param int $notificationID
     *
     * @return null|Notification
     */
    public function getNotification($notificationID)
    {
        return $this->hasNotification($notificationID) ? $this->notifications[$notificationID] : null;
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize()
    {
        return [
            'notifications' => $this->notifications,
            'cachedUntil'   => $this->formatDateTime($this->cachedUntil)
        ];
    }
}
