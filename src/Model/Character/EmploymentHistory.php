<?php

/*
 * This file is part of the Tz7\EveApiClient package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Model\Character;


use DateTimeInterface;
use Tz7\EveApiClient\Model\ApiResult;


class EmploymentHistory extends ApiResult
{
    /** @var int */
    private $recordID;

    /** @var int */
    private $corporationID;

    /** @var DateTimeInterface */
    private $startDate;

    /** @var DateTimeInterface */
    private $endDate;

    /**
     * @param int               $recordID
     * @param int               $corporationID
     * @param DateTimeInterface $startDate
     */
    public function __construct(
        $recordID,
        $corporationID,
        DateTimeInterface $startDate
    ) {
        $this->recordID      = $recordID;
        $this->corporationID = $corporationID;
        $this->startDate     = $startDate;
    }

    /**
     * @return int
     */
    public function getRecordID()
    {
        return $this->recordID;
    }

    /**
     * @return int
     */
    public function getCorporationID()
    {
        return $this->corporationID;
    }

    /**
     * @return DateTimeInterface
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @return DateTimeInterface
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param DateTimeInterface $endDate
     *
     * @return $this
     */
    public function setEndDate(DateTimeInterface $endDate = null)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'recordID'      => $this->recordID,
            'corporationID' => $this->corporationID,
            'startDate'     => $this->startDate ? $this->startDate->format('Y-m-d H:i:s') : null,
            'endDate'       => $this->endDate ? $this->endDate->format('Y-m-d H:i:s') : null,
        ];
    }
}
