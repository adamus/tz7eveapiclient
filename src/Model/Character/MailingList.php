<?php

/*
 * This file is part of the Tz7\EveApiClient package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Model\Character;


use Tz7\EveApiClient\Model\ApiResult;


class MailingList extends ApiResult
{
    /** @var int */
    private $listID;

    /** @var string */
    private $displayName;

    /**
     * @param int    $listID
     * @param string $displayName
     */
    public function __construct($listID, $displayName)
    {
        $this->listID      = $listID;
        $this->displayName = $displayName;
    }

    /**
     * @return string
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    /**
     * @return int
     */
    public function getListID()
    {
        return $this->listID;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'listID'      => $this->listID,
            'displayName' => $this->displayName
        ];
    }
}
