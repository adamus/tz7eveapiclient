<?php

/*
 * This file is part of the Tz7\EveApiClient package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Model\Character;


use DateTimeInterface;
use Tz7\EveApiClient\Model\ApiResult;


class MailMessage extends ApiResult
{
    /** @var int */
    private $messageID;

    /** @var int */
    private $senderID;

    /** @var string */
    private $senderName;

    /** @var DateTimeInterface */
    private $sentDate;

    /** @var string */
    private $title;

    /** @var int */
    private $toCorpOrAllianceID;

    /** @var int[] */
    private $toCharacterIDs;

    /** @var int */
    private $toListID;

    /**
     * @param int               $messageID
     * @param int               $senderID
     * @param string            $senderName
     * @param DateTimeInterface $sentDate
     * @param string            $title
     * @param int               $toCorpOrAllianceID
     * @param int[]             $toCharacterIDs
     * @param int               $toListID
     */
    public function __construct(
        $messageID,
        $senderID,
        $senderName,
        DateTimeInterface $sentDate,
        $title,
        $toCorpOrAllianceID,
        array $toCharacterIDs,
        $toListID
    ) {
        $this->messageID          = $messageID;
        $this->senderID           = $senderID;
        $this->senderName         = $senderName;
        $this->sentDate           = $sentDate;
        $this->title              = $title;
        $this->toCorpOrAllianceID = $toCorpOrAllianceID;
        $this->toCharacterIDs     = $toCharacterIDs;
        $this->toListID           = $toListID;
    }

    /**
     * @return integer
     */
    public function getMessageID()
    {
        return intval($this->messageID);
    }

    /**
     * @return integer
     */
    public function getSenderID()
    {
        return intval($this->senderID);
    }

    /**
     * @return string
     */
    public function getSenderName()
    {
        return $this->senderName;
    }

    /**
     * @return DateTimeInterface
     */
    public function getSentDate()
    {
        return $this->sentDate;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return integer
     */
    public function getToCorpOrAllianceID()
    {
        return intval($this->toCorpOrAllianceID);
    }

    /**
     * @return integer[]
     */
    public function getToCharacterIDs()
    {
        return !empty($this->toCharacterIDs) ? array_map('intval', explode(',', $this->toCharacterIDs)) : [];
    }

    /**
     * @return integer
     */
    public function getToListID()
    {
        return intval($this->toListID);
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'messageID'          => $this->messageID,
            'senderID'           => $this->senderID,
            'senderName'         => $this->senderName,
            'sentDate'           => $this->formatDateTime($this->sentDate),
            'title'              => $this->title,
            'toCorpOrAllianceID' => $this->toCorpOrAllianceID,
            'toCharacterIDs'     => $this->toCharacterIDs,
            'toListID'           => $this->toListID
        ];
    }
}
