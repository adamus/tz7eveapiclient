<?php

/*
 * This file is part of the Tz7\EveApiClient package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Model\Character;


use DateTimeInterface;
use Tz7\EveApiClient\Model\ApiResult;


class Notification extends ApiResult
{
    /** @var int */
    private $notificationID;

    /** @var int */
    private $typeID;

    /** @var int */
    private $senderID;

    /** @var string */
    private $senderName;

    /** @var DateTimeInterface */
    private $sentDate;

    /** @var bool */
    private $read;

    /**
     * @param int               $notificationID
     * @param int               $typeID
     * @param int               $senderID
     * @param string            $senderName
     * @param DateTimeInterface $sentDate
     * @param bool              $read
     */
    public function __construct(
        $notificationID,
        $typeID,
        $senderID,
        $senderName,
        DateTimeInterface $sentDate,
        $read
    ) {
        $this->notificationID = $notificationID;
        $this->typeID         = $typeID;
        $this->senderID       = $senderID;
        $this->senderName     = $senderName;
        $this->sentDate       = $sentDate;
        $this->read           = $read;
    }

    /**
     * @return int
     */
    public function getNotificationID()
    {
        return $this->notificationID;
    }

    /**
     * @return int
     */
    public function getTypeID()
    {
        return $this->typeID;
    }

    /**
     * @return int
     */
    public function getSenderID()
    {
        return $this->senderID;
    }

    /**
     * @return string
     */
    public function getSenderName()
    {
        return $this->senderName;
    }

    /**
     * @return DateTimeInterface
     */
    public function getSentDate()
    {
        return $this->sentDate;
    }

    /**
     * @return bool
     */
    public function getRead()
    {
        return $this->read;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'notificationID' => $this->notificationID,
            'typeID'         => $this->typeID,
            'senderID'       => $this->senderID,
            'senderName'     => $this->senderName,
            'sentDate'       => $this->formatDateTime($this->sentDate),
            'read'           => $this->read
        ];
    }
}
