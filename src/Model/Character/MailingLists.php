<?php

/*
 * This file is part of the Tz7\EveApiClient package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Model\Character;


use DateTimeInterface;
use Tz7\EveApiClient\Model\ApiResult;


class MailingLists extends ApiResult
{
    /** @var MailingList[] */
    private $mailingLists = [];

    /**
     * @param MailingList[]     $mailingLists
     * @param DateTimeInterface $cachedUntil
     */
    public function __construct(array $mailingLists, DateTimeInterface $cachedUntil = null)
    {
        $this->mailingLists = $mailingLists;
        $this->cachedUntil  = $cachedUntil;
    }

    /**
     * @return MailingList[]
     */
    public function getMailingLists()
    {
        return $this->mailingLists;
    }

    /**
     * @return array
     */
    public function getMailingListIds()
    {
        return array_keys($this->mailingLists);
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'mailingLists' => $this->mailingLists,
            'cachedUntil'  => $this->formatDateTime($this->cachedUntil)
        ];
    }
}
