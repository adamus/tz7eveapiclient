<?php

/*
 * This file is part of the Tz7\EveApiClient package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Model\Character;


use DateTimeInterface;
use Tz7\EveApiClient\Model\ApiResult;


class CharacterSheet extends ApiResult
{
    /** @var int */
    private $characterID;

    /** @var string */
    private $name;

    /** @var int */
    private $homeStationID;

    /** @var DateTimeInterface */
    private $dateOfBirth;

    /** @var string */
    private $race;

    /** @var string */
    private $bloodLine;

    /** @var string */
    private $ancestry;

    /** @var string */
    private $gender;

    /** @var string */
    private $corporationName;

    /** @var int */
    private $corporationID;

    /** @var string */
    private $allianceName;

    /** @var int */
    private $allianceID;

    /** @var string */
    private $factionName;

    /** @var int */
    private $factionID;

    /** @var string */
    private $cloneName;

    /** @var int */
    private $freeSkillPoints;

    /** @var int */
    private $freeRespecs;

    /** @var DateTimeInterface */
    private $cloneJumpDate;

    /** @var DateTimeInterface */
    private $lastRespecDate;

    /** @var DateTimeInterface */
    private $lastTimedRespec;

    /** @var DateTimeInterface */
    private $remoteStationDate;

    /** @var DateTimeInterface */
    private $jumpActivation;

    /** @var DateTimeInterface */
    private $jumpFatigue;

    /** @var DateTimeInterface */
    private $jumpLastUpdate;

    /** @var float */
    private $balance;

    /** @var CorporationRole[] */
    private $corporationRoles = [];

    /**
     * @param int               $characterID
     * @param string            $name
     * @param int               $homeStationID
     * @param DateTimeInterface $dateOfBirth
     * @param string            $race
     * @param string            $bloodLine
     * @param string            $ancestry
     * @param string            $gender
     * @param string            $corporationName
     * @param int               $corporationID
     * @param string            $allianceName
     * @param int               $allianceID
     * @param string            $factionName
     * @param int               $factionID
     * @param string            $cloneName
     * @param int               $freeSkillPoints
     * @param int               $freeRespecs
     * @param DateTimeInterface $cloneJumpDate
     * @param DateTimeInterface $lastRespecDate
     * @param DateTimeInterface $lastTimedRespec
     * @param DateTimeInterface $remoteStationDate
     * @param DateTimeInterface $jumpActivation
     * @param DateTimeInterface $jumpFatigue
     * @param DateTimeInterface $jumpLastUpdate
     * @param float             $balance
     * @param CorporationRole[] $corporationRoles
     */
    public function __construct(
        $characterID,
        $name,
        $homeStationID,
        DateTimeInterface $dateOfBirth,
        $race,
        $bloodLine,
        $ancestry,
        $gender,
        $corporationName,
        $corporationID,
        $allianceName,
        $allianceID,
        $factionName,
        $factionID,
        $cloneName,
        $freeSkillPoints,
        $freeRespecs,
        DateTimeInterface $cloneJumpDate,
        DateTimeInterface $lastRespecDate,
        DateTimeInterface $lastTimedRespec,
        DateTimeInterface $remoteStationDate,
        DateTimeInterface $jumpActivation,
        DateTimeInterface $jumpFatigue,
        DateTimeInterface $jumpLastUpdate,
        $balance,
        array $corporationRoles
    ) {
        $this->characterID       = $characterID;
        $this->name              = $name;
        $this->homeStationID     = $homeStationID;
        $this->dateOfBirth       = $dateOfBirth;
        $this->race              = $race;
        $this->bloodLine         = $bloodLine;
        $this->ancestry          = $ancestry;
        $this->gender            = $gender;
        $this->corporationName   = $corporationName;
        $this->corporationID     = $corporationID;
        $this->allianceName      = $allianceName;
        $this->allianceID        = $allianceID;
        $this->factionName       = $factionName;
        $this->factionID         = $factionID;
        $this->cloneName         = $cloneName;
        $this->freeSkillPoints   = $freeSkillPoints;
        $this->freeRespecs       = $freeRespecs;
        $this->cloneJumpDate     = $cloneJumpDate;
        $this->lastRespecDate    = $lastRespecDate;
        $this->lastTimedRespec   = $lastTimedRespec;
        $this->remoteStationDate = $remoteStationDate;
        $this->jumpActivation    = $jumpActivation;
        $this->jumpFatigue       = $jumpFatigue;
        $this->jumpLastUpdate    = $jumpLastUpdate;
        $this->balance           = $balance;
        $this->corporationRoles  = $corporationRoles;
    }

    /**
     * @return int
     */
    public function getCharacterID()
    {
        return intval($this->characterID);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getHomeStationID()
    {
        return intval($this->homeStationID);
    }

    /**
     * @return DateTimeInterface
     */
    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    /**
     * @return string
     */
    public function getRace()
    {
        return $this->race;
    }

    /**
     * @return string
     */
    public function getBloodLine()
    {
        return $this->bloodLine;
    }

    /**
     * @return string
     */
    public function getAncestry()
    {
        return $this->ancestry;
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @return string
     */
    public function getCorporationName()
    {
        return $this->corporationName;
    }

    /**
     * @return int
     */
    public function getCorporationID()
    {
        return intval($this->corporationID);
    }

    /**
     * @return string
     */
    public function getAllianceName()
    {
        return $this->allianceName;
    }

    /**
     * @return int
     */
    public function getAllianceID()
    {
        return intval($this->allianceID);
    }

    /**
     * @return string
     */
    public function getFactionName()
    {
        return $this->factionName;
    }

    /**
     * @return int
     */
    public function getFactionID()
    {
        return intval($this->factionID);
    }

    /**
     * @return string
     */
    public function getCloneName()
    {
        return $this->cloneName;
    }

    /**
     * @return int
     */
    public function getFreeSkillPoints()
    {
        return intval($this->freeSkillPoints);
    }

    /**
     * @return int
     */
    public function getFreeRespecs()
    {
        return intval($this->freeRespecs);
    }

    /**
     * @return DateTimeInterface
     */
    public function getCloneJumpDate()
    {
        return $this->cloneJumpDate;
    }

    /**
     * @return DateTimeInterface
     */
    public function getLastRespecDate()
    {
        return $this->lastRespecDate;
    }

    /**
     * @return DateTimeInterface
     */
    public function getLastTimedRespec()
    {
        return $this->lastTimedRespec;
    }

    /**
     * @return DateTimeInterface
     */
    public function getRemoteStationDate()
    {
        return $this->remoteStationDate;
    }

    /**
     * @return DateTimeInterface
     */
    public function getJumpActivation()
    {
        return $this->jumpActivation;
    }

    /**
     * @return DateTimeInterface
     */
    public function getJumpFatigue()
    {
        return $this->jumpFatigue;
    }

    /**
     * @return DateTimeInterface
     */
    public function getJumpLastUpdate()
    {
        return $this->jumpLastUpdate;
    }

    /**
     * @return float
     */
    public function getBalance()
    {
        return floatval($this->balance);
    }

    /**
     * @return CorporationRole[]
     */
    public function getCorporationRoles()
    {
        return $this->corporationRoles;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'characterID'       => $this->characterID,
            'name'              => $this->name,
            'homeStationID'     => $this->homeStationID,
            'DoB'               => $this->formatDateTime($this->dateOfBirth),
            'race'              => $this->race,
            'bloodLine'         => $this->bloodLine,
            'ancestry'          => $this->ancestry,
            'gender'            => $this->gender,
            'corporationName'   => $this->corporationName,
            'corporationID'     => $this->corporationID,
            'allianceName'      => $this->allianceName,
            'allianceID'        => $this->allianceID,
            'factionName'       => $this->factionName,
            'factionID'         => $this->factionID,
            'cloneName'         => $this->cloneName,
            'freeSkillPoints'   => $this->freeSkillPoints,
            'freeRespecs'       => $this->freeRespecs,
            'cloneJumpDate'     => $this->formatDateTime($this->cloneJumpDate),
            'lastRespecDate'    => $this->formatDateTime($this->lastRespecDate),
            'lastTimedRespec'   => $this->formatDateTime($this->lastTimedRespec),
            'remoteStationDate' => $this->formatDateTime($this->remoteStationDate),
            'jumpActivation'    => $this->formatDateTime($this->jumpActivation),
            'jumpFatigue'       => $this->formatDateTime($this->jumpFatigue),
            'jumpLastUpdate'    => $this->formatDateTime($this->jumpLastUpdate),
            'balance'           => $this->balance,
            'corporationRoles'  => $this->corporationRoles,
            'cachedUntil'       => $this->formatDateTime($this->cachedUntil)
        ];
    }
}
