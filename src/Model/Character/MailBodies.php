<?php

/*
 * This file is part of the Tz7\EveApiClient package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Model\Character;


use DateTimeInterface;
use Tz7\EveApiClient\Model\ApiResult;


class MailBodies extends ApiResult
{
    /** @var MailBody[] */
    protected $messages = [];

    /**
     * @param MailBody[]        $messages
     * @param DateTimeInterface $cachedUntil
     */
    public function __construct(array $messages, DateTimeInterface $cachedUntil = null)
    {
        $this->messages    = $messages;
        $this->cachedUntil = $cachedUntil;
    }

    /**
     * @return MailBody[]
     */
    public function getMailBodies()
    {
        return $this->messages;
    }

    /**
     * @param int $messageID
     *
     * @return bool
     */
    public function hasMessage($messageID)
    {
        return isset($this->messages[$messageID]);
    }

    /**
     * @param int $messageID
     *
     * @return null|MailBody
     */
    public function getMessage($messageID)
    {
        return $this->hasMessage($messageID) ? $this->messages[$messageID] : null;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'messages'    => $this->messages,
            'cachedUntil' => $this->getFormattedCachedUntil()
        ];
    }
}
