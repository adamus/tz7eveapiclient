<?php

/*
 * This file is part of the Tz7\EveApiClient package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Model\Character;


use Tz7\EveApiClient\Model\ApiResult;


class CorporationRole extends ApiResult
{
    /** @var int */
    private $roleID;

    /** @var string */
    private $roleName;

    /**
     * @param int    $roleID
     * @param string $roleName
     */
    public function __construct($roleID, $roleName)
    {
        $this->roleID   = $roleID;
        $this->roleName = $roleName;
    }

    /**
     * @return int
     */
    public function getRoleID()
    {
        return intval($this->roleID);
    }

    /**
     * @return string
     */
    public function getRoleName()
    {
        return $this->roleName;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'roleID'   => $this->roleID,
            'roleName' => $this->roleName
        ];
    }
}
