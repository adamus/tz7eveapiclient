<?php

/*
 * This file is part of the Tz7\EveApiClient package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Model\Corporation;


use DateTimeInterface;
use Tz7\EveApiClient\Model\ApiResult;


class CorporationSheet extends ApiResult
{
    /** @var int */
    private $corporationID;

    /** @var string */
    private $corporationName;

    /** @var string */
    private $ticker;

    /** @var int */
    private $allianceID;

    /** @var string */
    private $description;

    /** @var AllianceHistory[] */
    private $allianceHistory;

    /**
     * @param int               $corporationID
     * @param string            $corporationName
     * @param string            $ticker
     * @param int               $allianceID
     * @param string            $description
     * @param AllianceHistory[] $allianceHistory
     * @param DateTimeInterface $cachedUntil
     */
    public function __construct(
        $corporationID,
        $corporationName,
        $ticker,
        $allianceID,
        $description,
        array $allianceHistory,
        DateTimeInterface $cachedUntil = null
    ) {
        $this->corporationID   = $corporationID;
        $this->corporationName = $corporationName;
        $this->ticker          = $ticker;
        $this->allianceID      = $allianceID;
        $this->description     = $description;
        $this->allianceHistory = $allianceHistory;
        $this->cachedUntil     = $cachedUntil;
    }

    /**
     * @return integer
     */
    public function getCorporationID()
    {
        return intval($this->corporationID);
    }

    /**
     * @return string
     */
    public function getCorporationName()
    {
        return $this->corporationName;
    }

    /**
     * @return string
     */
    public function getTicker()
    {
        return $this->ticker;
    }

    /**
     * @return integer
     */
    public function getAllianceID()
    {
        return intval($this->allianceID);
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return strip_tags(str_replace('<br>', "\n", $this->description));
    }

    /**
     * @return AllianceHistory[]
     */
    public function getAllianceHistory()
    {
        return $this->allianceHistory;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'corporationID'   => $this->corporationID,
            'corporationName' => $this->corporationName,
            'ticker'          => $this->ticker,
            'allianceID'      => $this->allianceID,
            'allianceHistory' => $this->allianceHistory,
            'description'     => $this->getDescription(),
            'cachedUntil'     => $this->formatDateTime($this->cachedUntil)
        ];
    }
}
