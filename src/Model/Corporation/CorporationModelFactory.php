<?php

/*
 * This file is part of the Tz7\EveApiClient package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Model\Corporation;


use DateTimeInterface;
use Tz7\EveApiClient\Model\AbstractModelFactory;


class CorporationModelFactory extends AbstractModelFactory
{
    /**
     * @param int                      $corporationID
     * @param string                   $corporationName
     * @param string                   $ticker
     * @param int                      $allianceID
     * @param string                   $description
     * @param AllianceHistory[]        $allianceHistory
     * @param DateTimeInterface|string $cachedUntil
     *
     * @return CorporationSheet
     */
    public function createCorporationSheet(
        $corporationID,
        $corporationName,
        $ticker,
        $allianceID,
        $description,
        array $allianceHistory,
        $cachedUntil
    ) {
        $this->normalizeAllianceHistory($allianceHistory);

        return new CorporationSheet(
            $corporationID,
            $corporationName,
            $ticker,
            $allianceID,
            $description,
            $allianceHistory,
            $this->createLocalTime($cachedUntil)
        );
    }

    /**
     * @param int                      $recordID
     * @param int                      $allianceID
     * @param DateTimeInterface|string $startDate
     *
     * @return AllianceHistory
     */
    public function createAllianceHistory($recordID, $allianceID, $startDate)
    {
        return new AllianceHistory(
            $recordID,
            $allianceID,
            $this->createLocalTime($startDate)
        );
    }

    /**
     * @param AllianceHistory[] $allianceHistory
     */
    private function normalizeAllianceHistory(array &$allianceHistory)
    {
        usort(
            $allianceHistory,
            function (AllianceHistory $a, AllianceHistory $b)
            {
                return $a->getStartDate() > $b->getStartDate();
            }
        );

        /** @var AllianceHistory $lastMembership */
        $lastMembership = null;
        foreach ($allianceHistory as $employment)
        {
            if ($lastMembership)
            {
                $lastMembership->setEndDate(clone $employment->getStartDate());
            }
            $lastMembership = $employment;
        }
    }
}
