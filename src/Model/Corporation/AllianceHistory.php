<?php

/*
 * This file is part of the Tz7\EveApiClient package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Model\Corporation;


use DateTimeInterface;
use Tz7\EveApiClient\Model\ApiResult;


class AllianceHistory extends ApiResult
{
    /** @var int */
    private $recordID;

    /** @var int */
    private $allianceID;

    /** @var DateTimeInterface */
    private $startDate;

    /** @var DateTimeInterface */
    private $endDate;

    /**
     * @param int               $recordID
     * @param int               $allianceID
     * @param DateTimeInterface $startDate
     */
    public function __construct($recordID, $allianceID, DateTimeInterface $startDate)
    {
        $this->recordID   = $recordID;
        $this->allianceID = $allianceID;
        $this->startDate  = $startDate;
    }

    /**
     * @return int
     */
    public function getRecordID()
    {
        return $this->recordID;
    }

    /**
     * @return int
     */
    public function getAllianceID()
    {
        return $this->allianceID;
    }

    /**
     * @return DateTimeInterface
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @return DateTimeInterface
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param DateTimeInterface $endDate
     *
     * @return $this
     */
    public function setEndDate(DateTimeInterface $endDate = null)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize()
    {
        return [
            'recordID'   => $this->recordID,
            'allianceID' => $this->allianceID,
            'startDate'  => $this->formatDateTime($this->startDate),
            'endDate'    => $this->formatDateTime($this->endDate)
        ];
    }
}
