<?php

/*
 * This file is part of the Tz7\EveApiClient package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Model;


use Tz7\EveApiClient\Model\Account\AccountModelFactory;
use Tz7\EveApiClient\Model\Alliance\AllianceModelFactory;
use Tz7\EveApiClient\Model\Character\CharacterModelFactory;
use Tz7\EveApiClient\Model\Corporation\CorporationModelFactory;


class ModelFactoryProvider
{
    /** @var AccountModelFactory */
    private $accountModelFactory;

    /** @var CharacterModelFactory */
    private $characterModelFactory;

    /** @var CorporationModelFactory */
    private $corporationModelFactory;

    /** @var AllianceModelFactory */
    private $allianceModelFactory;

    public function __construct()
    {
        $this->accountModelFactory     = new AccountModelFactory();
        $this->characterModelFactory   = new CharacterModelFactory();
        $this->corporationModelFactory = new CorporationModelFactory();
        $this->allianceModelFactory    = new AllianceModelFactory();
    }

    /**
     * @return AccountModelFactory
     */
    public function getAccountModelFactory()
    {
        return $this->accountModelFactory;
    }

    /**
     * @return CharacterModelFactory
     */
    public function getCharacterModelFactory()
    {
        return $this->characterModelFactory;
    }

    /**
     * @return CorporationModelFactory
     */
    public function getCorporationModelFactory()
    {
        return $this->corporationModelFactory;
    }

    /**
     * @return AllianceModelFactory
     */
    public function getAllianceModelFactory()
    {
        return $this->allianceModelFactory;
    }
}
