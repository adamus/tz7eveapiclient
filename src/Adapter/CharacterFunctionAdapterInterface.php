<?php

/*
 * This file is part of the Tz7\EveApiClient package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Adapter;


use Tz7\EveApiClient\Model\Character\CharacterSheet;
use Tz7\EveApiClient\Model\Character\MailBodies;
use Tz7\EveApiClient\Model\Character\MailingLists;
use Tz7\EveApiClient\Model\Character\MailMessages;
use Tz7\EveApiClient\Model\Character\Notifications;
use Tz7\EveApiClient\Model\Character\NotificationTexts;


interface CharacterFunctionAdapterInterface
{
    /**
     * @param int $keyId
     * @param string $vCode
     * @param int $characterId
     * @return CharacterSheet
     */
    public function getCharacterSheet($keyId, $vCode, $characterId);

    /**
     * @param int $keyId
     * @param string $vCode
     * @param int $characterId
     * @return Notifications
     */
    public function getNotifications($keyId, $vCode, $characterId);

    /**
     * @param int $keyId
     * @param string $vCode
     * @param int $characterId
     * @param array $notificationIds
     * @return NotificationTexts
     */
    public function getNotificationTexts($keyId, $vCode, $characterId, array $notificationIds);

    /**
     * @param int $keyId
     * @param string $vCode
     * @param int $characterId
     * @return MailingLists
     */
    public function getMailingLists($keyId, $vCode, $characterId);

    /**
     * @param int $keyId
     * @param string $vCode
     * @param int $characterId
     * @return MailMessages
     */
    public function getMailMessages($keyId, $vCode, $characterId);

    /**
     * @param int $keyId
     * @param string $vCode
     * @param int $characterId
     * @param array $mailMessageIds
     * @return MailBodies
     */
    public function getMailBodies($keyId, $vCode, $characterId, array $mailMessageIds);
}
