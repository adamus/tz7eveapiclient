<?php

/*
 * This file is part of the Tz7\EveApiClient package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Adapter\Tz7RestApi;


use GuzzleHttp\Client;
use Symfony\Component\Routing\Router;
use Tz7\EveApiClient\Adapter\PublicFunctionAdapterInterface;
use Tz7\EveApiClient\Exception\ApiResultException;
use Tz7\EveApiClient\Model\Alliance\AllianceInfo;
use Tz7\EveApiClient\Model\Alliance\AllianceModelFactory;
use Tz7\EveApiClient\Model\Character\CharacterInfo;
use Tz7\EveApiClient\Model\Character\CharacterModelFactory;
use Tz7\EveApiClient\Model\Corporation\CorporationModelFactory;
use Tz7\EveApiClient\Model\Corporation\CorporationSheet;
use Tz7\EveApiClient\Model\ModelFactoryProvider;


class PublicRestAdapter implements PublicFunctionAdapterInterface
{
    /** @var CharacterModelFactory */
    private $characterModelFactory;

    /** @var CorporationModelFactory */
    private $corporationModelFactory;

    /** @var AllianceModelFactory */
    private $allianceModelFactory;

    /** @var Client */
    private $client;

    /** @var Router */
    private $router;

    /** @var string */
    private $endpoint;

    /**
     * @param ModelFactoryProvider $modelFactoryProvider
     * @param Router               $router
     * @param string               $endpoint
     */
    public function __construct(ModelFactoryProvider $modelFactoryProvider, Router $router, $endpoint)
    {
        $this->characterModelFactory   = $modelFactoryProvider->getCharacterModelFactory();
        $this->corporationModelFactory = $modelFactoryProvider->getCorporationModelFactory();
        $this->allianceModelFactory    = $modelFactoryProvider->getAllianceModelFactory();
        $this->client                  = new Client(['base_uri' => $endpoint]);
        $this->router                  = $router;
        $this->endpoint                = $endpoint;
    }

    /**
     * @param int $id
     *
     * @return string
     *
     * @throws ApiResultException
     */
    public function getEntityName($id)
    {
        $uri = $this->router->generate('eve_api_rest_public_entity_name', ['id' => $id]);

        return $this->fetchAndParse($uri);
    }

    /**
     * @param string $name
     *
     * @return string
     * @throws ApiResultException
     */
    public function getEntityId($name)
    {
        $uri = $this->router->generate('eve_api_rest_public_entity_id', ['name' => $name]);

        return $this->fetchAndParse($uri);
    }

    /**
     * @param int $id
     *
     * @return string
     *
     * @throws ApiResultException
     */
    public function getItemName($id)
    {
        $uri = $this->router->generate('eve_api_rest_public_item_name', ['id' => $id]);

        return $this->fetchAndParse($uri);
    }

    /**
     * @param string $name
     *
     * @return int
     */
    public function getItemId($name)
    {
        $uri = $this->router->generate('eve_api_rest_public_item_id', ['name' => $name]);

        return $this->fetchAndParse($uri);
    }

    /**
     * @param int $characterId
     *
     * @return CharacterInfo
     */
    public function getCharacterPublicInfo($characterId)
    {
        $uri    = $this->router->generate('eve_api_rest_public_character_info', ['characterId' => $characterId]);
        $result = $this->fetchAndParse($uri);

        $employmentHistory = array_map(
            function ($record)
            {
                return $this->characterModelFactory->createEmploymentHistory(
                    $record['recordID'],
                    $record['corporationID'],
                    $record['startDate']
                );
            },
            $result['employmentHistory']
        );

        return $this->characterModelFactory->createCharacterInfo(
            $result['characterID'],
            $result['characterName'],
            $result['corporationID'],
            $employmentHistory,
            $result['cachedUntil']
        );
    }

    /**
     * @param int $corporationId
     *
     * @return CorporationSheet
     */
    public function getCorporationPublicInfo($corporationId)
    {
        $uri    = $this->router->generate('eve_api_rest_public_corporation_info', ['corporationId' => $corporationId]);
        $result = $this->fetchAndParse($uri);

        $allianceHistory = array_map(
            function (array $record)
            {
                return $this->corporationModelFactory->createAllianceHistory(
                    $record['record_id'],
                    $record['alliance']['alliance_id'],
                    $record['start_date']
                );
            },
            array_filter(
                $result['allianceHistory'],
                function (array $record)
                {
                    return isset($record['alliance']);
                }
            )
        );

        return $this->corporationModelFactory->createCorporationSheet(
            $result['corporationID'],
            $result['corporationName'],
            $result['ticker'],
            $result['allianceID'],
            $result['description'],
            $allianceHistory,
            $result['cachedUntil']
        );
    }

    /**
     * @param $allianceId
     *
     * @return AllianceInfo
     */
    public function getAlliancePublicInfo($allianceId)
    {
        $uri    = $this->router->generate('eve_api_rest_public_alliance_info', ['allianceId' => $allianceId]);
        $result = $this->fetchAndParse($uri);

        return $this->allianceModelFactory->createAllianceInfo(
            $result['allianceID'],
            $result['name'],
            $result['shortName'],
            $result['executorCorpID'],
            $result['startDate']
        );
    }

    /**
     * @param string $uri
     *
     * @return mixed
     */
    protected function fetchAndParse($uri)
    {
        return $this->parse($uri, $this->fetch($uri));
    }

    /**
     * @param string $uri
     *
     * @return string
     */
    protected function fetch($uri)
    {
        return (string) $this->client->get($uri)->getBody();
    }

    /**
     * @param string $uri
     * @param string $raw
     *
     * @return mixed
     *
     * @throws ApiResultException
     */
    protected function parse($uri, $raw)
    {
        $result = json_decode($raw, true);

        if (!$result)
        {
            throw new ApiResultException(
                'Tz7/REST: ' . $uri,
                [
                    'raw'    => $raw,
                    'result' => $result
                ],
                'Could not parse API Response'
            );
        }

        return $result;
    }
}
