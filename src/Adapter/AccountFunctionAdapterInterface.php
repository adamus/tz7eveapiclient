<?php

/*
 * This file is part of the Tz7\EveApiClient package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Adapter;


use Tz7\EveApiClient\Model\Account\AccountCharacters;
use Tz7\EveApiClient\Model\Account\AccountStatus;
use Tz7\EveApiClient\Model\Account\ApiKeyInfo;


interface AccountFunctionAdapterInterface
{
    /**
     * @param integer $keyId
     * @param string $vCode
     * @return ApiKeyInfo
     */
    public function getApiKeyAccess($keyId, $vCode);

    /**
     * @param integer $keyId
     * @param string $vCode
     * @return AccountStatus
     */
    public function getAccountStatus($keyId,  $vCode);

    /**
     * @param integer $keyId
     * @param string $vCode
     * @return AccountCharacters
     */
    public function getAccountCharacters($keyId, $vCode);
}
