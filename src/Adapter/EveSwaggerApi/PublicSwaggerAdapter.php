<?php

/*
 * This file is part of the Tz7\EveApiClient package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Adapter\EveSwaggerApi;


use DateTimeImmutable;
use Tz7\EveApiClient\Adapter\PublicFunctionAdapterInterface;
use Tz7\EveApiClient\Model\Alliance\AllianceInfo;
use Tz7\EveApiClient\Model\Alliance\AllianceModelFactory;
use Tz7\EveApiClient\Model\Character\CharacterInfo;
use Tz7\EveApiClient\Model\Character\CharacterModelFactory;
use Tz7\EveApiClient\Model\Corporation\CorporationModelFactory;
use Tz7\EveApiClient\Model\Corporation\CorporationSheet;
use Tz7\EveApiClient\Model\ModelFactoryProvider;
use Tz7\EveSwaggerClient\Factory\ResourceFactory;
use Tz7\EveSwaggerClient\Resource\AllianceResource;
use Tz7\EveSwaggerClient\Resource\CharacterResource;
use Tz7\EveSwaggerClient\Resource\CorporationResource;
use Tz7\EveSwaggerClient\Resource\SearchResource;
use Tz7\EveSwaggerClient\Resource\UniverseResource;


class PublicSwaggerAdapter implements PublicFunctionAdapterInterface
{
    /** @var SearchResource */
    private $searchResource;

    /** @var UniverseResource */
    private $universeResource;

    /** @var CharacterResource */
    private $characterResource;

    /** @var CorporationResource */
    private $corporationResource;

    /** @var AllianceResource */
    private $allianceResource;

    /** @var CharacterModelFactory */
    private $characterModelFactory;

    /** @var CorporationModelFactory */
    private $corporationModelFactory;

    /** @var AllianceModelFactory */
    private $allianceModelFactory;

    /**
     * @param ResourceFactory      $resourceFactory
     * @param ModelFactoryProvider $modelFactoryProvider
     */
    public function __construct(ResourceFactory $resourceFactory, ModelFactoryProvider $modelFactoryProvider)
    {
        $this->searchResource          = $resourceFactory->createSearchResource();
        $this->universeResource        = $resourceFactory->createUniverseResource();
        $this->characterResource       = $resourceFactory->createCharacterResource();
        $this->corporationResource     = $resourceFactory->createCorporationResource();
        $this->allianceResource        = $resourceFactory->createAllianceResource();
        $this->characterModelFactory   = $modelFactoryProvider->getCharacterModelFactory();
        $this->corporationModelFactory = $modelFactoryProvider->getCorporationModelFactory();
        $this->allianceModelFactory    = $modelFactoryProvider->getAllianceModelFactory();
    }

    /**
     * @param int $id
     *
     * @return string
     */
    public function getEntityName($id)
    {
        $result = $this->universeResource->getNamesByIds([$id]);

        return $result[0]['name'];
    }

    /**
     * @param string $name
     *
     * @return int
     */
    public function getEntityId($name)
    {
        $result = $this->searchResource->getIdsByName(
            $name,
            [
                'alliance',
                'character',
                'corporation',
                'solar_system',
                'constellation',
                'region',
                'station'
            ],
            true
        );

        $merged = array_merge(...array_values($result));

        return reset($merged);
    }

    /**
     * @param int $id
     *
     * @return string
     */
    public function getItemName($id)
    {
        return $this->getEntityName($id);
    }

    /**
     * @param string $name
     *
     * @return int
     */
    public function getItemId($name)
    {
        $result = $this->searchResource->getIdsByName($name, ['inventory_type'], true);

        return $result['inventory_type'][0];
    }

    /**
     * @param int $characterId
     *
     * @return CharacterInfo
     */
    public function getCharacterPublicInfo($characterId)
    {
        $characterInfo      = $this->characterResource->getById($characterId);
        $corporationHistory = $this->characterResource->getCorporationHistoryById($characterId);

        $employmentHistory = array_map(
            function (array $record)
            {
                return $this->characterModelFactory->createEmploymentHistory(
                    $record['record_id'],
                    $record['corporation_id'],
                    $record['start_date']
                );
            },
            $corporationHistory
        );

        return $this->characterModelFactory->createCharacterInfo(
            $characterId,
            $characterInfo['name'],
            $characterInfo['corporation_id'],
            $employmentHistory,
            new DateTimeImmutable('now +3600 seconds')
        );
    }

    /**
     * @param int $corporationId
     *
     * @return CorporationSheet
     */
    public function getCorporationPublicInfo($corporationId)
    {
        $corporationInfo = $this->corporationResource->getById($corporationId);
        $allianceHistory = $this->corporationResource->getAllianceHistoryById($corporationId);

        $allianceHistory = array_map(
            function (array $record)
            {
                return $this->corporationModelFactory->createAllianceHistory(
                    $record['record_id'],
                    $record['alliance_id'],
                    $record['start_date']
                );
            },
            array_filter(
                $allianceHistory,
                function (array $record)
                {
                    return isset($record['alliance_id']);
                }
            )
        );

        return $this->corporationModelFactory->createCorporationSheet(
            $corporationId,
            $corporationInfo['name'],
            $corporationInfo['ticker'],
            isset($corporationInfo['alliance_id']) ? $corporationInfo['alliance_id'] : null,
            $corporationInfo['description'],
            $allianceHistory,
            new DateTimeImmutable('now +3600 seconds')
        );
    }

    /**
     * @param int $allianceId
     *
     * @return AllianceInfo
     */
    public function getAlliancePublicInfo($allianceId)
    {
        $result = $this->allianceResource->getById($allianceId);

        return $this->allianceModelFactory->createAllianceInfo(
            $allianceId,
            $result['name'],
            $result['ticker'],
            $result['executor_corporation_id'],
            $result['date_founded']
        );
    }
}
