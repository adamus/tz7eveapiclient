<?php

/*
 * This file is part of the Tz7\EveApiClient package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Adapter;


use Tz7\EveApiClient\Exception\ApiResultException;
use Tz7\EveApiClient\Model\Alliance\AllianceInfo;
use Tz7\EveApiClient\Model\Character\CharacterInfo;
use Tz7\EveApiClient\Model\Corporation\CorporationSheet;


interface PublicFunctionAdapterInterface
{
    /**
     * @param int $id
     * @return string
     * @throws ApiResultException
     */
    public function getEntityName($id);

    /**
     * @param string $name
     * @return string
     * @throws ApiResultException
     */
    public function getEntityId($name);

    /**
     * @param int $id
     * @return string
     * @throws ApiResultException
     */
    public function getItemName($id);

    /**
     * @param string $name
     * @return int
     */
    public function getItemId($name);

    /**
     * @param int $characterId
     * @return CharacterInfo
     */
    public function getCharacterPublicInfo($characterId);

    /**
     * @param int $corporationId
     * @return CorporationSheet
     */
    public function getCorporationPublicInfo($corporationId);

    /**
     * @param $allianceId
     * @return AllianceInfo|null
     */
    public function getAlliancePublicInfo($allianceId);
}
