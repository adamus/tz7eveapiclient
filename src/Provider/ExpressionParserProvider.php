<?php

/*
 * This file is part of the Tz7\EveApiBundle package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Provider;


use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\ExpressionLanguage\ExpressionFunctionProviderInterface;


class ExpressionParserProvider
{
    /** @var ExpressionLanguage */
    protected $expressionLanguage;

    public function __construct()
    {
        $this->expressionLanguage = new ExpressionLanguage();
    }

    /**
     * @param ExpressionFunctionProviderInterface $functionProvider
     */
    public function registerProvider(ExpressionFunctionProviderInterface $functionProvider)
    {
        $this->expressionLanguage->registerProvider($functionProvider);
    }

    /**
     * @param string $expression
     * @param array  $context
     *
     * @return string
     */
    public function evaluate($expression, array $context = [])
    {
        return $this->expressionLanguage->evaluate($expression, $context);
    }

    /**
     * @param string $expression
     * @param array  $context
     * @param string $regex
     *
     * @return string
     */
    public function processBlocks($expression, array $context = [], $regex = '\{([^\}]+)\}')
    {
        return preg_replace_callback(
            "~{$regex}~",
            function ($block) use ($context)
            {
                return $this->evaluate($block[1], $context);
            },
            $expression
        );
    }
}