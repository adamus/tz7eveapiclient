<?php

/*
 * This file is part of the Tz7\EveApiBundle package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Provider;


use Symfony\Component\ExpressionLanguage\ExpressionFunction;
use Symfony\Component\ExpressionLanguage\ExpressionFunctionProviderInterface;
use Tz7\EveApiClient\Resolver\TypeNameResolverInterface;


class ExpressionFunctionProvider implements ExpressionFunctionProviderInterface
{
    /** @var TypeNameResolverInterface */
    protected $typeNameResolver;

    /**
     * @param TypeNameResolverInterface $typeNameResolver
     */
    public function __construct(TypeNameResolverInterface $typeNameResolver)
    {
        $this->typeNameResolver = $typeNameResolver;
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new ExpressionFunction(
                'location',
                function ($string)
                {
                    return sprintf('location(%s)', $string);
                },
                function (array $values, $string)
                {
                    try
                    {
                        return $this->typeNameResolver->getLocationName($string);
                    }
                    catch (\Exception $ex)
                    {
                        return 'Unknown';
                    }
                }
            ),
            new ExpressionFunction(
                'entity',
                function ($string)
                {
                    return sprintf('entity(%s)', $string);
                },
                function (array $values, $string)
                {
                    try
                    {
                        return $this->typeNameResolver->getEntityName($string);
                    }
                    catch (\Exception $ex)
                    {
                        return 'Unknown';
                    }
                }
            ),
            new ExpressionFunction(
                'item',
                function ($string)
                {
                    return sprintf('item(%s)', $string);
                },
                function (array $values, $string)
                {
                    try
                    {
                        return $this->typeNameResolver->getItemName($string);
                    }
                    catch (\Exception $ex)
                    {
                        return 'Unknown';
                    }
                }
            ),
            new ExpressionFunction(
                'aggressor',
                function ()
                {
                    return 'aggressor()';
                },
                function (array $values)
                {
                    $aggressor = [];
                    if (isset($values['aggressorID']))
                    {
                        try
                        {
                            $aggressor[] = $this->typeNameResolver->getEntityName($values['aggressorID']);
                        }
                        catch (\Exception $ex)
                        {
                        }
                    }
                    if (isset($values['aggressorCorpID']))
                    {
                        try
                        {
                            $aggressor[] = $this->typeNameResolver->getEntityName($values['aggressorCorpID']);
                        }
                        catch (\Exception $ex)
                        {
                        }
                    }
                    if (isset($values['aggressorAllianceID']))
                    {
                        try
                        {
                            $aggressor[] = $this->typeNameResolver->getEntityName($values['aggressorAllianceID']);
                        }
                        catch (\Exception $ex)
                        {
                        }
                    }

                    return implode(', ', $aggressor);
                }
            ),
        ];
    }
}
