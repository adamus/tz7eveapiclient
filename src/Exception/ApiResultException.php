<?php

/*
 * This file is part of the Tz7\EveApiClient package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Exception;


class ApiResultException extends \RuntimeException
{
    /** @var string */
    protected $apiName;

    /** @var array */
    protected $parameters;

    public function __construct($apiName, array $parameters = [], $message = "", \Exception $previous = null)
    {
        parent::__construct($message, 0, $previous);
        $this->apiName = $apiName;
        $this->parameters = $parameters;
    }

    /**
     * @return string
     */
    public function getApiName()
    {
        return $this->apiName;
    }

    /**
     * @return array
     */
    public function getParameters()
    {
        return $this->parameters;
    }
}
