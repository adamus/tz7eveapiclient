<?php

/*
 * This file is part of the Tz7\EveApiClient package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Tests\Adapter;


use PHPUnit\Framework\TestCase;
use Tz7\EveApiClient\Adapter\PublicFunctionAdapterInterface;
use Tz7\EveApiClient\Model\Character\EmploymentHistory;


abstract class AbstractPublicFunctionIntegrationTest extends TestCase
{
    /** @var PublicFunctionAdapterInterface */
    protected $adapter;

    /**
     * @return PublicFunctionAdapterInterface
     */
    abstract protected function createFunctionAdapter();

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        $this->adapter = $this->createFunctionAdapter();
    }

    public function testEntityName()
    {
        $this->assertEquals('MPJW-6', $this->adapter->getEntityName(20000585));
    }

    public function testEntityId()
    {
        $this->assertEquals(20000585, $this->adapter->getEntityId('MPJW-6'));
    }

    public function testItemName()
    {
        $this->assertEquals('Hound', $this->adapter->getItemName(12034));
    }

    public function testItemId()
    {
        $this->assertEquals(1304, $this->adapter->getItemId('Adaptive Nano Plating I'));
    }

    public function testCharacterPublicInfo()
    {
        $characterInfo = $this->adapter->getCharacterPublicInfo($this->getParameter('EVE_API_TEST_CHARACTER_ID'));

        $this->assertEquals($this->getParameter('EVE_API_TEST_CHARACTER_NAME'), $characterInfo->getCharacterName());
        $this->assertNotEmpty($characterInfo->getEmploymentHistory());
    }

    public function testCorporationPublicInfo()
    {
        $corporationSheet = $this->adapter->getCorporationPublicInfo($this->getParameter('EVE_API_TEST_CORPORATION_ID'));

        $this->assertEquals($this->getParameter('EVE_API_TEST_CORPORATION_NAME'), $corporationSheet->getCorporationName());
        $this->assertEquals($this->getParameter('EVE_API_TEST_CORPORATION_TICKER'), $corporationSheet->getTicker());
        $this->assertEquals($this->getParameter('EVE_API_TEST_ALLIANCE_ID'), $corporationSheet->getAllianceID());
    }

    public function testAlliancePublicInfo()
    {
        $allianceInfo = $this->adapter->getAlliancePublicInfo($this->getParameter('EVE_API_TEST_ALLIANCE_ID'));

        $this->assertEquals($this->getParameter('EVE_API_TEST_ALLIANCE_NAME'), $allianceInfo->getName());
        $this->assertEquals($this->getParameter('EVE_API_TEST_ALLIANCE_TICKER'), $allianceInfo->getShortName());
    }

    /**
     * @param string $name
     *
     * @return string
     */
    protected function getParameter($name)
    {
        $value = getenv($name);

        if ($value === false)
        {
            throw new \InvalidArgumentException('Missing environment value: ' . $name);
        }

        return $value;
    }
}
