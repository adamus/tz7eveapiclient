<?php

/*
 * This file is part of the Tz7\EveApiClient package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Tests\Adapter;


use DateTimeInterface;
use PHPUnit\Framework\TestCase;
use Tz7\EveApiClient\Adapter\CharacterFunctionAdapterInterface;
use Tz7\EveApiClient\Model\Character;


abstract class AbstractCharacterFunctionIntegrationTest extends TestCase
{
    /**
     * @return CharacterFunctionAdapterInterface
     */
    abstract protected function getFunctionAdapter();

    /**
     * @return Character\Notifications
     */
    abstract protected function getNotifications();

    /**
     * @return Character\NotificationTexts
     */
    abstract protected function getNotificationTexts();

    /**
     * @return Character\MailingLists
     */
    abstract protected function getMailingLists();

    /**
     * @return Character\MailMessages
     */
    abstract protected function getMailMessages();

    /**
     * @return Character\MailBodies
     */
    abstract protected function getMailBodies();

    public function testNotifications()
    {
        $notifications = $this->getNotifications();

        $this->assertInstanceOf(Character\Notifications::class, $notifications);
        $this->assertNotEmpty($notifications->getNotifications());

        foreach ($notifications->getNotifications() as $notification)
        {
            $this->assertInstanceOf(Character\Notification::class, $notification);
            $this->assertNotEmpty($notification->getNotificationID());
        }

        return $notifications;
    }

    public function testNotificationTexts()
    {
        $notificationTexts = $this->getNotificationTexts();

        $this->assertInstanceOf(Character\NotificationTexts::class, $notificationTexts);
        $this->assertNotEmpty($notificationTexts->getNotificationTexts());

        foreach ($notificationTexts->getNotificationTexts() as $notificationText)
        {
            $this->assertInstanceOf(Character\NotificationText::class, $notificationText);
            $this->assertNotEmpty($notificationText->getParameters());
        }
    }

    public function testMailingList()
    {
        $mailingLists = $this->getMailingLists();

        $this->assertInstanceOf(Character\MailingLists::class, $mailingLists);
        $this->assertNotEmpty($mailingLists->getMailingListIds());

        foreach ($mailingLists->getMailingLists() as $mailingList)
        {
            $this->assertInstanceOf(Character\MailingList::class, $mailingList);
            $this->assertNotEmpty($mailingList->getListID());
            $this->assertNotEmpty($mailingList->getDisplayName());
        }
    }

    public function testMailMessages()
    {
        $mailMessages = $this->getMailMessages();

        $this->assertInstanceOf(Character\MailMessages::class, $mailMessages);
        $this->assertNotEmpty($mailMessages->getMessages());

        foreach ($mailMessages->getMessages() as $message)
        {
            $this->assertInstanceOf(Character\MailMessage::class, $message);
            $this->assertNotEmpty($message->getMessageID());
            $this->assertNotEmpty($message->getSenderID());
            $this->assertNotEmpty($message->getSenderName());
            $this->assertInstanceOf(DateTimeInterface::class, $message->getSentDate());
        }

        $mailBodies = $this->getMailBodies();

        $this->assertInstanceOf(Character\MailBodies::class, $mailBodies);
        $this->assertNotEmpty($mailBodies->getMailBodies());

        foreach ($mailBodies->getMailBodies() as $body)
        {
            $this->assertInstanceOf(Character\MailBody::class, $body);
            $this->assertNotEmpty($body->getMessageID());
        }
    }

    /**
     * @param string $name
     *
     * @return string
     */
    protected function getParameter($name)
    {
        $value = getenv($name);

        if ($value === false)
        {
            throw new \InvalidArgumentException('Missing environment value: ' . $name);
        }

        return $value;
    }
}
