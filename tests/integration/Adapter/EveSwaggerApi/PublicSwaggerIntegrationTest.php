<?php

/*
 * This file is part of the Tz7\EveApiClient package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Tests\Adapter\EveSwaggerApi;


use DateTimeInterface;
use Tz7\EveApiClient\Adapter\EveSwaggerApi\PublicSwaggerAdapter;
use Tz7\EveApiClient\Model\ModelFactoryProvider;
use Tz7\EveApiClient\Tests\Adapter\AbstractPublicFunctionIntegrationTest;
use Tz7\EveSwaggerClient\Factory\ResourceFactory;
use Tz7\EveSwaggerClient\SimpleParserClient;


class PublicSwaggerIntegrationTest extends AbstractPublicFunctionIntegrationTest
{
    /**
     * @inheritdoc
     */
    protected function createFunctionAdapter()
    {
        $config = [
            'scheme'  => 'https',
            'swagger' => 'https://esi.evetech.net/latest/swagger.json?datasource=tranquility',
            'headers' => [
                'User-Agent' => 'Testing Tz7\EveApiClient'
            ]
        ];

        $client               = new SimpleParserClient($config);
        $resourceFactory      = new ResourceFactory($client);
        $modelFactoryProvider = new ModelFactoryProvider();

        return new PublicSwaggerAdapter($resourceFactory, $modelFactoryProvider);
    }

    public function testCorporationPublicInfo()
    {
        $corporationSheet = $this->adapter->getCorporationPublicInfo($this->getParameter('EVE_API_TEST_CORPORATION_ID'));

        $this->assertEquals($this->getParameter('EVE_API_TEST_CORPORATION_NAME'), $corporationSheet->getCorporationName());
        $this->assertEquals($this->getParameter('EVE_API_TEST_CORPORATION_TICKER'), $corporationSheet->getTicker());
        $this->assertEquals($this->getParameter('EVE_API_TEST_ALLIANCE_ID'), $corporationSheet->getAllianceID());

        $this->assertNotEmpty($corporationSheet->getAllianceHistory());

        $wasMemberOfAllianceIds = [];

        foreach ($corporationSheet->getAllianceHistory() as $allianceHistory)
        {
            $wasMemberOfAllianceIds[] = $allianceHistory->getAllianceID();

            $this->assertNotEmpty($allianceHistory->getRecordID());
            $this->assertInstanceOf(DateTimeInterface::class, $allianceHistory->getStartDate());
        }

        $this->assertContains($this->getParameter('EVE_API_TEST_ALLIANCE_ID'), $wasMemberOfAllianceIds);
    }
}
