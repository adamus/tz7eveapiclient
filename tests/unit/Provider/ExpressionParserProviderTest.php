<?php

/*
 * This file is part of the Tz7\EveApiClient package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiClient\Tests\Provider;


use PHPUnit\Framework\TestCase;
use Tz7\EveApiClient\Provider\ExpressionFunctionProvider;
use Tz7\EveApiClient\Provider\ExpressionParserProvider;
use Tz7\EveApiClient\Resolver\TypeNameResolver;


class ExpressionParserProviderTest extends TestCase
{
    public function testEvaluateItem()
    {
        $typeNameResolverMock = $this
            ->getMockBuilder(TypeNameResolver::class)
            ->disableOriginalConstructor()
            ->setMethods(['getItemName'])
            ->getMock();

        $typeNameResolverMock
            ->method('getItemName')
            ->with(32226)
            ->willReturn('Territorial Claim Unit');

        $expressionParserProvider = $this->getExpressionParserProvider($typeNameResolverMock);

        $result = $expressionParserProvider->evaluate('item(32226)');
        $this->assertEquals('Territorial Claim Unit', $result);
    }

    public function testEvaluateLocation()
    {
        $typeNameResolverMock = $this
            ->getMockBuilder(TypeNameResolver::class)
            ->disableOriginalConstructor()
            ->setMethods(['getLocationName'])
            ->getMock();

        $typeNameResolverMock
            ->method('getLocationName')
            ->with(20000585)
            ->willReturn('MPJW-6');

        $expressionParserProvider = $this->getExpressionParserProvider($typeNameResolverMock);

        $result = $expressionParserProvider->evaluate('location(20000585)');
        $this->assertEquals('MPJW-6', $result);

        $result = $expressionParserProvider->evaluate(
            'location(solarSystemID)',
            [
                'solarSystemID' => 20000585
            ]
        );
        $this->assertEquals('MPJW-6', $result);
    }

    public function testBlocksProcessing()
    {
        $typeNameResolverMock = $this
            ->getMockBuilder(TypeNameResolver::class)
            ->disableOriginalConstructor()
            ->setMethods(['getLocationName'])
            ->getMock();

        $typeNameResolverMock
            ->method('getLocationName')
            ->with(30003997)
            ->willReturn('YF-6L1');

        $expressionParserProvider = $this->getExpressionParserProvider($typeNameResolverMock);

        $expression = 'Citadel being anchored in {location(solarsystemID)} by {ownerCorpName}';
        $context    = [
            'solarsystemID' => 30003997,
            'ownerCorpName' => 'Coortus Legio'
        ];
        $excepted   = 'Citadel being anchored in YF-6L1 by Coortus Legio';

        $result = $expressionParserProvider->processBlocks($expression, $context);
        $this->assertEquals($excepted, $result);
    }

    /**
     * @param TypeNameResolver $typeNameResolver
     *
     * @return ExpressionParserProvider
     */
    private function getExpressionParserProvider(TypeNameResolver $typeNameResolver)
    {
        $expressionParserProvider = new ExpressionParserProvider();
        $expressionParserProvider->registerProvider(new ExpressionFunctionProvider($typeNameResolver));

        return $expressionParserProvider;
    }
}
