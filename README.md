TZ-7: EveApiClient
======


This repository is part of the [TZ-7 project](https://bitbucket.org/adamus/tz-7), 
providing object-oriented layer to [EVE Online API](https://developers.eveonline.com/).

* Currently under development (focused on public calls).
* The primary goal: [Common interface](docs/adapters/common_interface.md)
* For examples please see tests.

