TZ-7: EveApiClient
======

## Common interface

The basic idea with putting an additional layer between the API calls and the services (models) of 
the application is to be able to switch between different implementations and endpoints without having
to rewrite core functionalities.

### Public function adapter 

Adapters implementing the [PublicFunctionAdapterInterface](../../src/Adapter/PublicFunctionAdapterInterface.php)
should cover all public API calls. *(This may be broken in smaller pieces with ESI.)*

* [PublicSwaggerAdapter](../../src/Adapter/EveSwaggerApi/PublicSwaggerAdapter.php) is using 
[TZ-7\EveSwaggerClient](https://bitbucket.org/adamus/tz7eveswaggerclient) to communicate with the new
EVE API endpoints.
* [PublicRestAdapter](../../src/Adapter/Tz7RestApi/PublicRestAdapter.php) is for using 
[TZ-7\EveApiBundle](https://bitbucket.org/adamus/tz7eveapibundle) as a reverse proxy cache. 


## Character function adapter

Adapters implementing the [CharacterFunctionAdapterInterface](../../src/Adapter/CharacterFunctionAdapterInterface.php)
should cover all private API calls of a character.

